#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_system.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#include "driver/spi_master.h"
char sync;
void vApplicationIdleHook( void ){



}

#define  SSID "LogN"
#define  PASS "salama9876543210salama"

/* FreeRTOS event group to signal when we are connected & ready to make a request */
static EventGroupHandle_t wifi_event_group;

/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */
const int CONNECTED_BIT = BIT0;


static const char *TAG = "TEMPERATURE_NODE";

//#define MAX6675_MISO 19
//#define MAX6675_SCK 18
//#define MAX6675_CS 5
//#define MAX6675_SPI_HOST VSPI_HOST
#define MAX6675_MISO 19
#define MAX6675_SCK 18
#define MAX6675_CS 5
#define MAX6675_SPI_HOST VSPI_HOST //HOST HSPI_HOST
#define GPIO_OUTPUT_PIN_MASK  ((1<<MAX6675_CS))

#ifndef HIGH
    #define HIGH 1
#endif
#ifndef LOW
    #define LOW 0
#endif

static spi_device_handle_t spi_handle; // SPI handle.


static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch(event->event_id) {
    case SYSTEM_EVENT_STA_START:
        esp_wifi_connect();
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        /* This is a workaround as ESP32 WiFi libs don't currently
           auto-reassociate. */
        esp_wifi_connect();
        xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
        break;
    default:
        break;
    }
    return ESP_OK;
}

static void initialise_wifi(void)
{
    tcpip_adapter_init();
    wifi_event_group = xEventGroupCreate();
    ESP_ERROR_CHECK( esp_event_loop_init(event_handler, NULL) );
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
    ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = SSID,
            .password = PASS,
        },
    };
    ESP_LOGI(TAG, "Setting WiFi configuration SSID %s...", wifi_config.sta.ssid);
    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK( esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
    ESP_ERROR_CHECK( esp_wifi_start() );
}

static void init_gpio() {
	gpio_config_t io_conf;

    io_conf.intr_type = GPIO_PIN_INTR_DISABLE; //disable interrupt
    io_conf.mode = GPIO_MODE_OUTPUT; //set as output mode
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_MASK; //bit mask of the pins
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE; //disable pull-down mode
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;   //disable pull-up mode
    ESP_LOGD(TAG, "gpio_config");
    gpio_config(&io_conf); //configure GPIO with the given settings

}
//********************8
uint16_t data,rawtemp,temp=0;
    spi_bus_config_t bus_config;
	spi_device_interface_config_t dev_config;
	spi_transaction_t trans_word;

void init_spi(){
	ESP_LOGD(TAG, "readMax6675 start");

    gpio_set_level(MAX6675_CS, HIGH); // MAX6675_CS
	bus_config.sclk_io_num   = MAX6675_SCK; // CLK
	bus_config.mosi_io_num   = -1; // MOSI not used
	bus_config.miso_io_num   = MAX6675_MISO; // MISO
	bus_config.quadwp_io_num = -1; // not used
	bus_config.quadhd_io_num = -1; // not used
    ESP_LOGD(TAG, "spi_bus_initialize");
	ESP_ERROR_CHECK(spi_bus_initialize(MAX6675_SPI_HOST, &bus_config, 2));

	dev_config.address_bits     = 0;
	dev_config.command_bits     = 0;
	dev_config.dummy_bits       = 0;
	dev_config.mode             = 0; // SPI_MODE0
	dev_config.duty_cycle_pos   = 0;
	dev_config.cs_ena_posttrans = 0;
	dev_config.cs_ena_pretrans  = 0;
	//dev_config.clock_speed_hz   = 2000000;  // 2 MHz
	dev_config.clock_speed_hz   = 10000;  // 10 kHz
	dev_config.spics_io_num     = -1; // CS External
	dev_config.flags            = 0; // SPI_MSBFIRST
	dev_config.queue_size       = 100;
	dev_config.pre_cb           = NULL;
	dev_config.post_cb          = NULL;
 //   ESP_LOGD(TAG, "spi_bus_add_device");
//	ESP_ERROR_CHECK(spi_bus_add_device(MAX6675_SPI_HOST, &dev_config, &spi_handle));


}
//*********************************
void disable_spi(){
	 ESP_LOGD(TAG, "spi_bus_remove_device");
	    ESP_ERROR_CHECK(spi_bus_remove_device(spi_handle));
	    ESP_LOGD(TAG, "spi_bus_free");
	    ESP_ERROR_CHECK(spi_bus_free(MAX6675_SPI_HOST));
}
//*****************************
uint16_t readMax6675() {
	uint16_t data,rawtemp,temp=0;
    spi_bus_config_t bus_config;
	spi_device_interface_config_t dev_config;
	spi_transaction_t trans_word;

	ESP_LOGD(TAG, "readMax6675 start");

    gpio_set_level(MAX6675_CS, HIGH); // MAX6675_CS
	bus_config.sclk_io_num   = MAX6675_SCK; // CLK
	bus_config.mosi_io_num   = -1; // MOSI not used
	bus_config.miso_io_num   = MAX6675_MISO; // MISO
	bus_config.quadwp_io_num = -1; // not used
	bus_config.quadhd_io_num = -1; // not used
    ESP_LOGD(TAG, "spi_bus_initialize");
	ESP_ERROR_CHECK(spi_bus_initialize(MAX6675_SPI_HOST, &bus_config, 2));

	dev_config.address_bits     = 0;
	dev_config.command_bits     = 0;
	dev_config.dummy_bits       = 0;
	dev_config.mode             = 0; // SPI_MODE0
	dev_config.duty_cycle_pos   = 0;
	dev_config.cs_ena_posttrans = 0;
	dev_config.cs_ena_pretrans  = 0;
	//dev_config.clock_speed_hz   = 2000000;  // 2 MHz
	dev_config.clock_speed_hz   = 10000;  // 10 kHz
	dev_config.spics_io_num     = -1; // CS External
	dev_config.flags            = 0; // SPI_MSBFIRST
	dev_config.queue_size       = 100;
	dev_config.pre_cb           = NULL;
	dev_config.post_cb          = NULL;
    ESP_LOGD(TAG, "spi_bus_add_device");
	ESP_ERROR_CHECK(spi_bus_add_device(MAX6675_SPI_HOST, &dev_config, &spi_handle));


    ESP_LOGD(TAG, "MAX6675_CS prepare");
    gpio_set_level(MAX6675_CS, LOW); // MAX6675_CS prepare
    vTaskDelay(500 / portTICK_RATE_MS);  // see MAX6675 datasheet

    rawtemp = 0x000;
    data = 0x000;  // write dummy

	//trans_word.address   = 0;
	//trans_word.command   = 0;
	trans_word.flags     = 0;
	trans_word.length    = 8 * 2; // Total data length, in bits NOT number of bytes.
	trans_word.rxlength  = 0; // (0 defaults this to the value of ``length``)
	trans_word.tx_buffer = &data;
	trans_word.rx_buffer = &rawtemp;
    ESP_LOGD(TAG, "spi_device_transmit");
	ESP_ERROR_CHECK(spi_device_transmit(spi_handle, &trans_word));

	gpio_set_level(MAX6675_CS, HIGH); // MAX6675_CS prepare

    temp = ((((rawtemp & 0x00FF) << 8) | ((rawtemp & 0xFF00) >> 8))>>3)*25;
   // temp = ((rawtemp)>>3)*25;
	ESP_LOGI(TAG, "readMax6675 spiReadWord=%x temp=%d.%d",rawtemp,temp/100,temp%100);

    ESP_LOGD(TAG, "spi_bus_remove_device");
    ESP_ERROR_CHECK(spi_bus_remove_device(spi_handle));
    ESP_LOGD(TAG, "spi_bus_free");
    ESP_ERROR_CHECK(spi_bus_free(MAX6675_SPI_HOST));

    return temp;

}
//*************************
uint16_t Temperature=0;

static void temp_get_task(void *pvParameters) {

	while(1) {

		temp=readMax6675();


		if(temp==0){
			disable_spi();
			goto END;
		}
	   ESP_LOGI(TAG, "Temp Task temp=%d.%d",temp/100,temp%100);
		vTaskDelay(1000 / portTICK_RATE_MS);
    }
    END:
	   //disable_spi();
	   ESP_LOGI(TAG, "End Temp Task");

    vTaskDelete(NULL);
}
//**************************************
#define M_Controller "192.168.1.111"
#define M_PORT       2000
char Data[20],M_Reply[10];
//*********************************
void client_task(void *p)
{
int counter=0;

struct sockaddr_in server;
int s;
  xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT,
								false, true, portMAX_DELAY);

		ESP_LOGI(TAG, "Enter Client Task");
//receive from Data concatenation task using Queue or Global Variable
int i;
while(1)
{

	printf("Send Temeprature Data to the Main Controller\n");
	sprintf(Data,"T,%d.%d",temp/100,temp%100);

		server.sin_addr.s_addr = inet_addr(M_Controller);
		server.sin_family = AF_INET;
		server.sin_port = htons( M_PORT );

		s = socket(AF_INET, SOCK_STREAM, 0);
		//unsigned long mode=1;
		//ioctlsocket(s,FIONBIO,&mode);
	   if(s < 0)
	   {
		printf("Error 2\n");
		   ESP_LOGE(TAG, "... Failed to allocate socket.");
		   close(s);
		  // counter++;
		   //continue;
		   vTaskDelay(100 / portTICK_PERIOD_MS);
	   }
	   ESP_LOGI(TAG, "... allocated socket\r\n");
	  if(connect(s , (struct sockaddr *)&server , sizeof(server))!= 0)
	  {
		printf("Error connect\n");
		   ESP_LOGE(TAG, "... socket connect failed errno=%d", errno);
		   close(s);
		   //counter++;
		   vTaskDelay(100 / portTICK_PERIOD_MS);
		  // continue;
	   }
	   printf("Connected\n");
	   ESP_LOGI(TAG, "... connected");

	   if (send(s,Data, strlen(Data),0) < 0)
	   {
		 printf("Error Send\n");
		   ESP_LOGE(TAG, "... socket send failed");
		   close(s);

		   vTaskDelay(100 / portTICK_PERIOD_MS);
		  // continue;
	   }

		 bzero(M_Reply, sizeof(M_Reply));
		 read(s, M_Reply, sizeof(M_Reply)-1);
		 printf("Server Reply:%s Size%d\n",M_Reply,strlen(M_Reply));

		 close(s);
		 bzero(Data, strlen(Data));

		vTaskDelay(2000/portTICK_PERIOD_MS);

}
}//task
//********************************************************
#define delm ","
void server_4Main(void *p)
{
	struct sockaddr_in clientAddress;
	struct sockaddr_in serverAddress;
	socklen_t clientAddressLength;

	char recvbuf[10];

	int sock;
	int rc;
	int clientSock;
	char *id, *Order, *token,*sleepPeriod;
	while(1)
	{

	xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT,
						false, true, portMAX_DELAY);

	//ESP_LOGI(TAG, "Connected to AP");

	// Create a socket that we will listen upon.
	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sock < 0) {
		//ESP_LOGE(TAG, "socket: %d %s", sock, strerror(errno));
		goto END;
	}
	// Bind our server socket to a port.
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddress.sin_port = htons(5000);

	rc  = bind(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress));
	if (rc < 0) {
		//ESP_LOGE(TAG, "bind: %d %s", rc, strerror(errno));
		goto END;
	}

	// Flag the socket as listening for new connections.
	rc = listen(sock, 30);
	if (rc < 0) {
		//ESP_LOGE(TAG, "listen: %d %s", rc, strerror(errno));
		goto END;
	}

	while (1)
	{

		// Listen for a new client connection.
		clientAddressLength = sizeof(clientAddress);
		clientSock = accept(sock, (struct sockaddr *)&clientAddress, &clientAddressLength);
		if (clientSock < 0)
		{
			//ESP_LOGE(TAG, "accept: %d %s (%d)", clientSock, strerror(errno),errno);
			if (errno!=23)
			{
				goto END;
			}

		}
		bzero(recvbuf,sizeof(recvbuf));
	    read(clientSock, recvbuf, sizeof(recvbuf)-1);
		printf("\nReceived Data  From Main_Controller=%s  size: %d\n",recvbuf,strlen(recvbuf));
		 //send the receive buffer to the SD card task
		close(clientSock);


	  if(recvbuf[0]=='M')
		{
		    token = strtok(recvbuf, delm);
		 	id = token;
		 	token = strtok(NULL, delm);
		 	Order = token;
		 	token = strtok(NULL, delm);
		 	sleepPeriod=token;
		     id=NULL;
		     vTaskDelay(100 / portTICK_PERIOD_MS);
       }

	}
	END:
	 printf(" Socket task 1 out\n");
	vTaskDelay(3000 / portTICK_PERIOD_MS);
 }
}
void app_main() {

    nvs_flash_init();
    initialise_wifi();
    init_gpio();

    xTaskCreatePinnedToCore(&temp_get_task, "temp_get_task", 8192, NULL, 4, NULL,0);
    xTaskCreatePinnedToCore(&client_task, "client_task", 8192, NULL, 5, NULL,1);
}
