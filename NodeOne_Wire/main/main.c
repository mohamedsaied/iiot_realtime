/*
 * main.c
 *
 *  Created on: Apr 27, 2018
 *      Author: Eng. Mohamed
 *
 *      NodeID W
 *      WireState 0/1=Off/ON
 *      Frame:NodeID,WireState i.e (W,1)
 */
#include "main.h"

const int CONNECTED_BIT = BIT0;

#define M_Controller "192.168.1.111"
char M_Con_Reply[200],Data[200];
char sync=0;
void vApplicationIdleHook( void ){



}
//*************Client_task****************************

void client_task(void *p){

struct sockaddr_in server;
int s;
  xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT,
								false, true, portMAX_DELAY);

		ESP_LOGI(TAG, "Enter Client Task");
//receive from Data concatenation task using Queue or Global Variable

while(1)
{
	while(!sync){

	}
	printf("Data to be Send: %s,size of Data=%d\n",Data, strlen(Data));

		server.sin_addr.s_addr = inet_addr(M_Controller);
		server.sin_family = AF_INET;
		server.sin_port = htons( 1000 );

		s = socket(AF_INET, SOCK_STREAM, 0);
	   if(s < 0)
	   {
		printf("Error 2\n");
		   ESP_LOGE(TAG, "... Failed to allocate socket.");
		   close(s);
           //continue;
		   vTaskDelay(100 / portTICK_PERIOD_MS);
	   }
	   ESP_LOGI(TAG, "... allocated socket\r\n");
	  if(connect(s , (struct sockaddr *)&server , sizeof(server))!= 0)
	  {
		printf("Error connect\n");
		   ESP_LOGE(TAG, "... socket connect failed errno=%d", errno);
		   close(s);
		   vTaskDelay(100 / portTICK_PERIOD_MS);
       }
	   printf("Connected\n");
	   ESP_LOGI(TAG, "... connected");

	   if (send(s,Data, strlen(Data),0) < 0)
	   {
		 printf("Error Send\n");
		   ESP_LOGE(TAG, "... socket send failed");
		   close(s);

		   vTaskDelay(100 / portTICK_PERIOD_MS);
	   }


		 bzero(M_Con_Reply, sizeof(M_Con_Reply));
		 read(s, M_Con_Reply, sizeof(M_Con_Reply)-1);
		 printf("Server Reply:%s Size%d\n",M_Con_Reply,strlen(M_Con_Reply));

		 close(s);
		 bzero(Data, strlen(Data));
		 sync=0;
		vTaskDelay(2000/portTICK_PERIOD_MS);

}
}//task



//*************ON/OFF BUTTON*****************************************
void check(void *p){
	gpio_set_direction(GPIO_4, GPIO_MODE_INPUT);
	gpio_set_direction(GPIO_21, GPIO_MODE_INPUT);
	gpio_set_direction(GPIO_22, GPIO_MODE_OUTPUT);


	int value=0;
	while(1){
		Data[0]='W';
		Data[1]=',';
        value= gpio_get_level(GPIO_21);
        if(value!=0){

        	Data[2]='1';
        	printf("Filament is valid\n");
             gpio_set_level(GPIO_22,1);

        }
        else{
        	Data[2]='0';
        	printf("Filament breakage detected\n");
            gpio_set_level(GPIO_22,0);

        }
        sync++;
		vTaskDelay(500/portTICK_PERIOD_MS);
	}
}
//****************************************
static esp_err_t wifi_event_handler(void *ctx, system_event_t *event)
{
	switch(event->event_id) {
	case SYSTEM_EVENT_STA_START:
		esp_wifi_connect();
		break;
	case SYSTEM_EVENT_STA_GOT_IP:
		xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
		break;
	case SYSTEM_EVENT_STA_DISCONNECTED:
		/* This is a workaround as ESP32 WiFi libs don't currently
		   auto-reassociate. */
		esp_wifi_connect();
		xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
		break;
	default:
		break;
	}
	printf("555555555555 I am here in wifi handler 555555555555");
	return ESP_OK;
}

//*****************************************
void wifi_conn_init(void)
{

	tcpip_adapter_init();
	tcpip_adapter_dhcpc_stop(TCPIP_ADAPTER_IF_STA);
	 tcpip_adapter_ip_info_t ipInfo;

	 inet_pton(AF_INET, DEVICE_IP, &ipInfo.ip);
	 inet_pton(AF_INET, DEVICE_GW, &ipInfo.gw);
	 inet_pton(AF_INET, DEVICE_NETMASK, &ipInfo.netmask);
	 tcpip_adapter_set_ip_info(TCPIP_ADAPTER_IF_STA, &ipInfo);

	wifi_event_group = xEventGroupCreate();
	ESP_ERROR_CHECK( esp_event_loop_init(wifi_event_handler, NULL) );
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
	ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );

	wifi_config_t wifi_config = {
		.sta = {
			.ssid =  SSID,
			.password =PASS,
		},
	};
	ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
	ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );

	ESP_LOGI(TAG, "start the WIFI SSID:[%s] password:[%s]\n", SSID, PASS);
	ESP_ERROR_CHECK( esp_wifi_start() );

}

//*****************************************
/*
 * Because there is a bootloader takes the app_main function and puts it
 * in [ int main(void)] function
 */
void app_main(void){
	//First step is to initlize the flash
	 //Initialize NVS

	    esp_err_t ret = nvs_flash_init();
	    if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
	      ESP_ERROR_CHECK(nvs_flash_erase());
	      ret = nvs_flash_init();
	    }
	    ESP_ERROR_CHECK(ret);

	    //initlize wifi
		wifi_conn_init();
	xTaskCreatePinnedToCore(check, "check", 4000, NULL, 3, NULL,0);
	xTaskCreatePinnedToCore(client_task, "client_task", 4000, NULL, 4, NULL,1);

}
