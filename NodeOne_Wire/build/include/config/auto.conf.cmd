deps_config := \
	/e/workspace/ESP_Environment/esp-idf/components/app_trace/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/aws_iot/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/bt/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/esp32/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/ethernet/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/fatfs/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/freertos/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/heap/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/libsodium/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/log/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/lwip/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/mbedtls/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/openssl/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/pthread/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/spi_flash/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/spiffs/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/tcpip_adapter/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/wear_levelling/Kconfig \
	/e/workspace/ESP_Environment/esp-idf/components/bootloader/Kconfig.projbuild \
	/e/workspace/ESP_Environment/esp-idf/components/esptool_py/Kconfig.projbuild \
	/e/workspace/ESP_Environment/esp-idf/components/partition_table/Kconfig.projbuild \
	/e/workspace/ESP_Environment/esp-idf/Kconfig

include/config/auto.conf: \
	$(deps_config)


$(deps_config): ;
